#
#
# intertable.py
# This is solving a general problem I've had for a while which is that I need a
# text dump of intersections between various datasets.
#
#
import math
import random

class IntersectionTable(object):
    def gen_counts(self):
        """
        gen_counts(self): 
        Updates self.counts with all the count values for every tag in the 
        list of tags
        """
        for tag in self.tags:
            count = self.read_function(self.tags[tag])
        self.counts(append(count))

    def uniform_id(tag_a, tag_b):
        # returns a sorted tuple of tag_a and tag_b so I don't
        # have to run the same intersection twice.
        if tag_a < tag_b:
            return(tag_a, tag_b)
        else:
            return(tag_b, tag_a)
        
    def gen_intersects(self):
        # A note here -- we are implicitly relying on ordering in uniform_id() is the
        # same as the ordering in sorted().  Without that we won't get a nice
        # triangular output
        s = sorted(self.tags.keys())
        for tag_a in s:
            for tag_b in s:
                id = IntersectionTable.uniform_id(tag_a, tag_b)
                if not id in self.intersects:
                    self.intersects[id] = self.intersect_function(self.tags[tag_a], self.tags[tag_b])
    
                    
    def _calc_widths(self):
        """
        calc_widths(self)
        Internal utility function that calculates the widths for the title and values columns
        """
        title_width = 1 + (sorted(list(map(len, self.tags.keys()))))[-1]
        #
        # Okay, this is a bit opaque -- 1 + log-base is a quick trick to find how many digits you need in base x
        # So this just takes all the intersects, pulls them out, logs them to get column count, sorts in order, picks the
        # widest, then adds 2 for aesthetics
        column_width = 1 + sorted(list(map(lambda x:1 if x <= 0 else int(1 + math.log10(x)), list(self.intersects.values()))))[-1]
        if title_width < column_width:
            return column_width
        else:
            return title_width
    
    def table_plot(self):
        # First iteration is using fixed values, will fix on second
        s = sorted(self.tags.keys())
        width = self._calc_widths()
        format_string = ("%%%ds" % width) + (("%%%ds" % width) * len(s))
        titles = tuple(["Tag"] + s)
        print(format_string % titles)
        done_ids = set()
        for i in s:
            values = [i]
            for j in s:
                uid = IntersectionTable.uniform_id(i,j)
                if not uid in done_ids: 
                    if not uid in self.intersects:
                        values.append('N/A')
                    else:
                        values.append(str(self.intersects[uid]))
                else:
                    values.append(' ')
                done_ids.add(uid)
            print(format_string % tuple(values))
        
    def truncate_tags(self):
        """
        This is a cosmetic function because some of my tags are excessively long.  
        It remaps the table with truncated tags based on an internal truncation parameter.
        """
        nt = {}
        for i in self.tags.keys():
            nt[i[0:self.title_trunc]] = self.tags[i]
        self.tags = nt 
            
    def __init__(self, tags, read_function, intersect_function):
        self.tags = tags
        self.read_function = read_function
        self.intersect_function = intersect_function
        self.counts = []
        self.intersects = {}
        self.title_trunc = 6
        

def test():
    # Generate some random sets
    rsets = {str(i): random.choices(range(0, 100), k = 40) for i in range(0, 10)}
    test_rf = lambda x: len(list(x))
    test_if = lambda x, y: len(set(x).intersection(set(y)))
    it = IntersectionTable(rsets, test_rf, test_if)
    it.gen_intersects()
    it.table_plot()
if __name__ == '__main__':
    test()
    
        
