#!/usr/bin/env python3
#
# crossverify.py
#
# This is a utility function which takes the intersection
# of each set in a directory and prints the output; this is
# used as a verification -- the system should be a partition!
import intertable
import os
import sys
import glob

def read_function(filename):
    read_pipe = os.popen('rwsetcat --count-ip %s' % filename)
    raw_count = read_pipe.read()
    result = int(raw_count)
    return result

def intersect_function(filename_a, filename_b):
    read_pipe = os.popen('rwsettool --intersect %s %s | rwsetcat --count-ip' % (filename_a, filename_b))
    raw_count = read_pipe.read()
    result = int(raw_count)
    return result

if __name__ == '__main__':
    working_directory = sys.argv[1]
    set_files = glob.glob(os.path.join(working_directory, '*.set'))
    tags = {}
    for i in set_files:
        tags[os.path.basename(i.split('.')[0])] = i
    t = intertable.IntersectionTable(tags, read_function, intersect_function)
    t.truncate_tags()
    t.gen_intersects()
    t.table_plot()
    
